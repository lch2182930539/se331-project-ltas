import {Component, OnInit} from '@angular/core';
import {StudentService} from '../service/student.service';
import {FormBuilder, Validators} from '@angular/forms';
import {MatSnackBar} from '@angular/material';
import {Router} from '@angular/router';

@Component({
  selector: 'app-student-login',
  templateUrl: './student-login.component.html',
  styleUrls: ['./student-login.component.scss']
})
export class StudentLoginComponent implements OnInit {
  constructor(
    private studentService: StudentService,
    private fb: FormBuilder,
    private snackBar: MatSnackBar,
    private router: Router
  ) {
  }

  loginForm = this.fb.group({
    email: this.fb.control('', [Validators.required, Validators.email]),
    password: this.fb.control('', Validators.required)
  });

  ngOnInit() {
    if (this.studentService.loginStudent) {
      this.router.navigateByUrl('/student');
    }
  }

  onSubmit({email, password}: { email: string; password: string }) {
    const ret = this.studentService.login(email, password);
    if (ret.state === 'ok') {
      this.router.navigateByUrl('/student');
    } else {
      alert(ret.msg);
    }
  }
}

