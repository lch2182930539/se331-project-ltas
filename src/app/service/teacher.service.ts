import {Injectable} from '@angular/core';
import {exampleModelData, Teacher} from '../model';

@Injectable({
  providedIn: 'root'
})
export class TeacherService {
  private static teachersKey = 'teachers';

  constructor() {
    this.load();
  }

  teachers: Teacher[] = [];

  currentTeacher: Teacher | null = null;

  findTeacherById(id: string) {
    return this.teachers.find(x => x.id === id);
  }

  save() {
    localStorage.setItem(
      TeacherService.teachersKey,
      JSON.stringify(this.teachers)
    );
  }

  load() {
    const students = localStorage.getItem(TeacherService.teachersKey);
    if (students) {
      this.teachers = JSON.parse(students);
    } else {
      this.teachers = exampleModelData.teachers;
    }
  }
}
