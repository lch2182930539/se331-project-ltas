import {Injectable} from '@angular/core';
import {Activity, exampleModelData, Student, StudentActivity} from '../model';

@Injectable({
  providedIn: 'root'
})
export class ActivityService {
  private static activitiesKey = 'activities';
  private static studentActivityListKey = 'studentActivityListKey';

  constructor() {
    this.load();
  }

  activities: Activity[] = [];
  studentActivityList: StudentActivity[] = [];

  apply(activity: Activity, student: Student) {
    this.studentActivityList.push({
      activityId: activity.id,
      studentId: student.id,
      state: 'pending'
    });
    this.save();
  }

  add(activity: Activity) {
    this.activities.push(activity);
    this.save();
  }

  isAlreadyApplied(activityId: number, studentId: string) {
    return !!this.studentActivityList.find(x => x.activityId === activityId && x.studentId === studentId);
  }

  save() {
    localStorage.setItem(
      ActivityService.activitiesKey,
      JSON.stringify(this.activities)
    );
    localStorage.setItem(
      ActivityService.studentActivityListKey,
      JSON.stringify(this.studentActivityList)
    );
  }

  load() {
    const students = localStorage.getItem(ActivityService.activitiesKey);
    const studentActivityList = localStorage.getItem(ActivityService.studentActivityListKey);
    if (students) {
      this.activities = JSON.parse(students);
    } else {
      this.activities = exampleModelData.activities;
    }
    if (studentActivityList) {
      this.studentActivityList = JSON.parse(studentActivityList);
    } else {
      this.studentActivityList = exampleModelData.studentActivityList;
    }
  }
}
