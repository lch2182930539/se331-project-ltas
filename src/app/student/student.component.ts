import {Component, OnInit} from '@angular/core';
import {StudentService} from '../service/student.service';
import {Router} from '@angular/router';
import {ActivityService} from '../service/activity.service';

@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.scss']
})
export class StudentComponent implements OnInit {

  constructor(private studentService: StudentService, private router: Router, private activityService:ActivityService) {
  }

  ngOnInit() {
    if (!this.studentService.loginStudent) {
      this.router.navigateByUrl('/student/login');
    }
  }

}
