export interface Activity {
  id: number;
  name: string;
  location: string;
  description: string;
  period: string;
  datetime: string;
  teacherId: string;
}

export interface Student {
  id: string;
  name: string;
  surname: string;
  birth: string;
  email: string;
  password: string;
  confirmed: boolean;
}

export interface StudentActivity {
  studentId: string;
  activityId: number;
  state: 'pending' | 'confirm'
}

export interface Teacher {
  id: string;
  name: string;
}

export interface Manager {
  id: string;
  name: string;
}

export const exampleModelData = {
  activities: [
    {
      id: 1,
      name: 'AAAAAAAAAAA',
      location: 'no no no no no no no no',
      description:
        'and on and on and on and on and on and on and on and on and onand on and on and onand on and on and on and on and on and on',
      period: '2019-09-20 12:00',
      datetime: '2019-09-22 14:30',
      teacherId: '1'
    },
    {
      id: 2,
      name: 'BBBBBBB',
      location: 'qqq q qqq qq qqqq',
      description:
        'and on and on and on and on and on and on and on and on and onand on and on and onand on and on and on and on and on and on',
      period: '2019-09-21 12:00',
      datetime: '2019-09-23 14:30',
      teacherId: '1'
    },
    {
      id: 3,
      name: 'CCCCCCCCCCCCCC',
      location: 'sss sssssssss sss',
      description:
        'and on and on and on and on and on and on and on and on and onand on and on and onand on and on and on and on and on and on',
      period: '2019-09-22 12:00',
      datetime: '2019-09-24 14:30',
      teacherId: '2'
    },
    {
      id: 4,
      name: 'DDDDDDDDD',
      location: 'no no no no no no no no',
      description:
        'and on and on and on and on and on and on and on and on and onand on and on and onand on and on and on and on and on and on',
      period: '2019-09-18 12:00',
      datetime: '2019-09-27 14:30',
      teacherId: '1'
    },
    {
      id: 5,
      name: 'EEEEEEEEEE',
      location: 'qqq q qqq qq qqqq',
      description:
        'and on and on and on and on and on and on and on and on and onand on and on and onand on and on and on and on and on and on',
      period: '2019-09-29 12:00',
      datetime: '2019-09-30 14:30',
      teacherId: '1'
    },
    {
      id: 6,
      name: 'FFFFFFFFF',
      location: 'sss sssssssss sss',
      description:
        'and on and on and on and on and on and on and on and on and onand on and on and onand on and on and on and on and on and on',
      period: '2019-09-30 12:00',
      datetime: '2019-10-02 14:30',
      teacherId: '2'
    },
  ] as Activity[],
  students: [
    {
      name: 'Any',
      surname: 'Li',
      birth: '1999-02-11',
      id: '2019082201',
      email: 'any@q.com',
      password: 'aaaaaa',
      confirmed: true
    },
    {
      name: 'Bold',
      surname: 'Xi',
      birth: '2000-02-11',
      id: '2019082202',
      email: 'bold@q.com',
      password: 'aaaaaa',
      confirmed: true
    }
  ] as Student[],
  studentActivityList: [

    {
      studentId: '2019082201',
      activityId: 5,
      state: 'pending'
    },
    {
      studentId: '2019082202',
      activityId: 1,
      state: 'pending'
    },
    {
      studentId: '2019082202',
      activityId: 2,
      state: 'confirm'
    },
    {
      studentId: '2019082202',
      activityId: 5,
      state: 'pending'
    },
    {
      studentId: '2019082202',
      activityId: 3,
      state: 'confirm'
    },
    {
      studentId: '2019082202',
      activityId: 4,
      state: 'confirm'
    },
  ] as StudentActivity[],
  teachers: [
    {
      name: 'Wang',
      id: '1'
    },
    {
      name: 'Liu',
      id: '2'
    }
  ] as Teacher[],
  managers: [
    {
      name: 'Zhang',
      id: '1'
    },
    {
      name: 'Xing',
      id: '2'
    }
  ] as Manager[]
};

export type Ret<T = any> = {
  state: 'ok' | 'fail';
  msg?: string;
} & T;
