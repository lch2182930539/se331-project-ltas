import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {StudentActivityAllComponent} from './student-activity-all.component';

describe('StudentActivityAllComponent', () => {
  let component: StudentActivityAllComponent;
  let fixture: ComponentFixture<StudentActivityAllComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [StudentActivityAllComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentActivityAllComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
